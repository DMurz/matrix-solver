﻿namespace MatrixSolver.Code;

public class SolveController
{
    private readonly MatrixView _matrixView;
    private readonly VectorView _bVectorView;
    private readonly VectorView _xVectorView;

    public SolveController(MatrixView matrixView, VectorView bVectorView, VectorView xVectorView)
    {
        _matrixView = matrixView;
        _bVectorView = bVectorView;
        _xVectorView = xVectorView;
    }

    public void Solve()
    {
        var matrix = _matrixView.GetMatrix();
        var bVector = _bVectorView.GetVector();
        if (matrix == null || bVector == null) OnError("Solve error!");
        
        var xVector = matrix.Solve(bVector);
        _xVectorView.SetValues(xVector);
    }
    
    private void OnError(object errorMessage) =>
        MessageBox.Show(errorMessage.ToString(), "Error in solver", MessageBoxButtons.OK, MessageBoxIcon.Error);
}