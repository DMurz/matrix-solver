﻿namespace MatrixSolver.Code;

public class MatrixController
{
    private readonly MatrixView _view;

    public MatrixController(MatrixView view)
    {
        _view = view;
    }

    public double? CalculateDeterminant()
    {
        var matrix = _view.GetMatrix();
        if (matrix == null)
            return null;
        return matrix.Determinant();
    }
}