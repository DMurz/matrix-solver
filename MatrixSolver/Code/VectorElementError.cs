﻿namespace MatrixSolver.Code;

public class VectorElementError
{
    private readonly int _i;

    public VectorElementError(int i)
    {
        _i = i;
    }
    
    public override string ToString() => 
        $"Error in B vector [{_i}]";
}