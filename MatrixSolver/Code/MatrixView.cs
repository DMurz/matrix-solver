﻿using MathNet.Numerics.LinearAlgebra;

namespace MatrixSolver.Code;

public class MatrixView
{
    private readonly int _size;
    private readonly Func<string, TextBox?> _findTextBox;
    private readonly TextBoxView[,] _textBoxViews;

    public MatrixView(int size, Func<string, TextBox?> findTextBox)
    {
        _size = size;
        _findTextBox = findTextBox;
        _textBoxViews = new TextBoxView[size, size];
    }

    public void Initialize()
    {
        for (var i = 0; i < _size; i++)
        for (var j = 0; j < _size; j++)
        {
            var name = $"Matrix_{i}_{j}";
            var textBox = _findTextBox.Invoke(name);
            if (textBox == null) throw new Exception($"Element with name: {name} doesn't exist on form");
            _textBoxViews[i, j] = new TextBoxView(textBox, new MatrixElementError(i, j), OnError);
        }
    }

    public void FillRandom()
    {
        var random = new Random();
        for (var i = 0; i < _size; i++)
        for (var j = 0; j < _size; j++)
            _textBoxViews[i, j].Value = random.Next(0, 100);
    }
    
    public void FillSequence()
    {
        var count = 0;
        for (var i = 0; i < _size; i++)
        for (var j = 0; j < _size; j++)
        {
            _textBoxViews[i, j].Value = count;
            count++;
        }
    }

    public void Clear()
    {
        for (var i = 0; i < _size; i++)
        for (var j = 0; j < _size; j++)
            _textBoxViews[i, j].Clear();
    }

    public Matrix<double> GetMatrix()
    {
        var matrix = Matrix<double>.Build.Dense(_size, _size);
        for (var i = 0; i < _size; i++)
        for (var j = 0; j < _size; j++)
        {
            var value = _textBoxViews[i, j].Value;
            if (!value.HasValue)
            {
                _textBoxViews[i, j].ShowError();
                return null;
            }

            matrix[i, j] = value.Value;
        }

        return matrix;
    }

    private void OnError(object errorMessage) =>
        MessageBox.Show(errorMessage.ToString(), "Error in matrix", MessageBoxButtons.OK, MessageBoxIcon.Error);
}