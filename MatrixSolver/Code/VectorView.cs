﻿using MathNet.Numerics.LinearAlgebra;

namespace MatrixSolver.Code;

public class VectorView
{
    private readonly int _size;
    private readonly Func<string, TextBox?> _findTextBox;
    private readonly string _pattern;
    private readonly TextBoxView[] _textBoxViews;

    public VectorView(int size, Func<string, TextBox?> findTextBox, string pattern)
    {
        _size = size;
        _findTextBox = findTextBox;
        _pattern = pattern;
        _textBoxViews = new TextBoxView[size];
    }

    public void Initialize()
    {
        for (var i = 0; i < _size; i++)
        {
            var name = $"{_pattern}_{i}";
            var textBox = _findTextBox.Invoke(name);
            if (textBox == null) throw new Exception($"Element with name: {name} doesn't exist on form");
            _textBoxViews[i] = new TextBoxView(textBox, new VectorElementError(i), OnError);
        }
    }
    
    public void FillRandom()
    {
        var random = new Random();
        for (var i = 0; i < _size; i++)
            _textBoxViews[i].Value = random.Next(0, 100);
    }
    
    public void FillSequence()
    {
        for (var i = 0; i < _size; i++) 
            _textBoxViews[i].Value = i;
    }

    public void Clear()
    {
        for (var i = 0; i < _size; i++)
            _textBoxViews[i].Clear();
    }

    public Vector<double> GetVector()
    {
        var vector = Vector<double>.Build.Dense(5);
        for (var i = 0; i < _size; i++)
        {
            var value = _textBoxViews[i].Value;
            if (!value.HasValue)
            {
                _textBoxViews[i].ShowError();
                return null;
            }

            vector[i] = value.Value;
        }

        return vector;
    }

    public void SetValues(Vector<double> values)
    {
        if (values.Count != _size) return;
        for (var i = 0; i < _size; i++)
            _textBoxViews[i].Value = values[i];
    }

    private void OnError(object errorMessage) =>
        MessageBox.Show(errorMessage.ToString(), "Error in vector", MessageBoxButtons.OK, MessageBoxIcon.Error);
}