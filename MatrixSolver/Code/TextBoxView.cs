﻿namespace MatrixSolver.Code;

public class TextBoxView
{
    public double? Value
    {
        get
        {
            if (!ReadValue()) return null;
            return _value;
        }
        set => _textBox.Text = $"{value.Value}";
    }

    private double _value;

    private readonly TextBox _textBox;
    private readonly object _errorInfo;
    private readonly Action<object> _onError;

    public TextBoxView(TextBox textBox, object errorInfo, Action<object> onError)
    {
        _textBox = textBox;
        _errorInfo = errorInfo;
        _onError = onError;
    }

    public void ShowError() =>
        _onError?.Invoke(_errorInfo);

    public void Clear() =>
        _textBox.Text = "";

    private bool ReadValue() =>
        double.TryParse(_textBox.Text, out _value);
}