﻿namespace MatrixSolver.Code;

public class MatrixElementError
{
    private readonly int _i;
    private readonly int _j;

    public MatrixElementError(int i, int j)
    {
        _i = i;
        _j = j;
    }

    public override string ToString() => 
        $"Error in element [{_i}, {_j}]";
}