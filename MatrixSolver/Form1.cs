using MatrixSolver.Code;

namespace MatrixSolver;

public partial class Form1 : Form
{
    private MatrixView _matrixView;
    private MatrixController _matrixController;
    private VectorView _bVectorView;
    private VectorView _xVectorView;
    private SolveController _solveController;

    public Form1()
    {
        InitializeComponent();
        InitializeMatrixView();
    }

    private void SolveButton_Click(object sender, EventArgs e)
    {
        _xVectorView.Clear();
        var determinant = _matrixController.CalculateDeterminant();
        if (determinant.HasValue)
            DeterminantText.Text = $"{determinant.Value}";
        if (determinant.Value == 0)
            OnError("Error in matrix");
        else
            _solveController.Solve();
    }

    private void RandomMatrix_Click(object sender, EventArgs e)
    {
        _matrixView.FillRandom();
        _bVectorView.FillRandom();
    }

    private void ClearButton_Click(object sender, EventArgs e)
    {
        _matrixView.Clear();
        _bVectorView.Clear();
        _xVectorView.Clear();
        DeterminantText.Text = "";
    }

    private void FillSequence_Click(object sender, EventArgs e)
    {
        _matrixView.FillSequence();
        _bVectorView.FillSequence();
    }

    private void InitializeMatrixView()
    {
        _matrixView = new MatrixView(5, GetElementById<TextBox>);
        _matrixController = new MatrixController(_matrixView);
        _bVectorView = new VectorView(5, GetElementById<TextBox>, "B");
        _xVectorView = new VectorView(5, GetElementById<TextBox>, "X");
        _solveController = new SolveController(_matrixView, _bVectorView, _xVectorView);

        _matrixView.Initialize();
        _bVectorView.Initialize();
        _xVectorView.Initialize();
    }

    private T? GetElementById<T>(string name) where T : Control
    {
        var elements = Controls.Find(name, false);
        if (elements.Length == 0) return null;
        return elements[0] as T;
    }

    private void OnError(object errorMessage) =>
        MessageBox.Show(errorMessage.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
}