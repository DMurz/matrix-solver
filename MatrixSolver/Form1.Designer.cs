﻿namespace MatrixSolver;

partial class Form1
{
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }

        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.SolveButton = new System.Windows.Forms.Button();
            this.Matrix_0_0 = new System.Windows.Forms.TextBox();
            this.Matrix_0_1 = new System.Windows.Forms.TextBox();
            this.Matrix_0_2 = new System.Windows.Forms.TextBox();
            this.Matrix_0_3 = new System.Windows.Forms.TextBox();
            this.Matrix_0_4 = new System.Windows.Forms.TextBox();
            this.Matrix_1_4 = new System.Windows.Forms.TextBox();
            this.Matrix_1_3 = new System.Windows.Forms.TextBox();
            this.Matrix_1_2 = new System.Windows.Forms.TextBox();
            this.Matrix_1_1 = new System.Windows.Forms.TextBox();
            this.Matrix_1_0 = new System.Windows.Forms.TextBox();
            this.Matrix_2_4 = new System.Windows.Forms.TextBox();
            this.Matrix_2_3 = new System.Windows.Forms.TextBox();
            this.Matrix_2_2 = new System.Windows.Forms.TextBox();
            this.Matrix_2_1 = new System.Windows.Forms.TextBox();
            this.Matrix_2_0 = new System.Windows.Forms.TextBox();
            this.Matrix_3_4 = new System.Windows.Forms.TextBox();
            this.Matrix_3_3 = new System.Windows.Forms.TextBox();
            this.Matrix_3_2 = new System.Windows.Forms.TextBox();
            this.Matrix_3_1 = new System.Windows.Forms.TextBox();
            this.Matrix_3_0 = new System.Windows.Forms.TextBox();
            this.Matrix_4_4 = new System.Windows.Forms.TextBox();
            this.Matrix_4_3 = new System.Windows.Forms.TextBox();
            this.Matrix_4_2 = new System.Windows.Forms.TextBox();
            this.Matrix_4_1 = new System.Windows.Forms.TextBox();
            this.Matrix_4_0 = new System.Windows.Forms.TextBox();
            this.DeterminantText = new System.Windows.Forms.TextBox();
            this.RandomMatrix = new System.Windows.Forms.Button();
            this.B_4 = new System.Windows.Forms.TextBox();
            this.B_3 = new System.Windows.Forms.TextBox();
            this.B_2 = new System.Windows.Forms.TextBox();
            this.B_1 = new System.Windows.Forms.TextBox();
            this.B_0 = new System.Windows.Forms.TextBox();
            this.X_4 = new System.Windows.Forms.TextBox();
            this.X_3 = new System.Windows.Forms.TextBox();
            this.X_2 = new System.Windows.Forms.TextBox();
            this.X_1 = new System.Windows.Forms.TextBox();
            this.X_0 = new System.Windows.Forms.TextBox();
            this.ClearButton = new System.Windows.Forms.Button();
            this.FillSequence = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SolveButton
            // 
            this.SolveButton.Location = new System.Drawing.Point(759, 241);
            this.SolveButton.Name = "SolveButton";
            this.SolveButton.Size = new System.Drawing.Size(75, 23);
            this.SolveButton.TabIndex = 0;
            this.SolveButton.Text = "Solve";
            this.SolveButton.UseVisualStyleBackColor = true;
            this.SolveButton.Click += new System.EventHandler(this.SolveButton_Click);
            // 
            // Matrix_0_0
            // 
            this.Matrix_0_0.Location = new System.Drawing.Point(51, 41);
            this.Matrix_0_0.Name = "Matrix_0_0";
            this.Matrix_0_0.Size = new System.Drawing.Size(100, 23);
            this.Matrix_0_0.TabIndex = 1;
            // 
            // Matrix_0_1
            // 
            this.Matrix_0_1.Location = new System.Drawing.Point(157, 41);
            this.Matrix_0_1.Name = "Matrix_0_1";
            this.Matrix_0_1.Size = new System.Drawing.Size(100, 23);
            this.Matrix_0_1.TabIndex = 2;
            // 
            // Matrix_0_2
            // 
            this.Matrix_0_2.Location = new System.Drawing.Point(263, 41);
            this.Matrix_0_2.Name = "Matrix_0_2";
            this.Matrix_0_2.Size = new System.Drawing.Size(100, 23);
            this.Matrix_0_2.TabIndex = 3;
            // 
            // Matrix_0_3
            // 
            this.Matrix_0_3.Location = new System.Drawing.Point(369, 41);
            this.Matrix_0_3.Name = "Matrix_0_3";
            this.Matrix_0_3.Size = new System.Drawing.Size(100, 23);
            this.Matrix_0_3.TabIndex = 4;
            // 
            // Matrix_0_4
            // 
            this.Matrix_0_4.Location = new System.Drawing.Point(475, 41);
            this.Matrix_0_4.Name = "Matrix_0_4";
            this.Matrix_0_4.Size = new System.Drawing.Size(100, 23);
            this.Matrix_0_4.TabIndex = 5;
            // 
            // Matrix_1_4
            // 
            this.Matrix_1_4.Location = new System.Drawing.Point(475, 70);
            this.Matrix_1_4.Name = "Matrix_1_4";
            this.Matrix_1_4.Size = new System.Drawing.Size(100, 23);
            this.Matrix_1_4.TabIndex = 10;
            // 
            // Matrix_1_3
            // 
            this.Matrix_1_3.Location = new System.Drawing.Point(369, 70);
            this.Matrix_1_3.Name = "Matrix_1_3";
            this.Matrix_1_3.Size = new System.Drawing.Size(100, 23);
            this.Matrix_1_3.TabIndex = 9;
            // 
            // Matrix_1_2
            // 
            this.Matrix_1_2.Location = new System.Drawing.Point(263, 70);
            this.Matrix_1_2.Name = "Matrix_1_2";
            this.Matrix_1_2.Size = new System.Drawing.Size(100, 23);
            this.Matrix_1_2.TabIndex = 8;
            // 
            // Matrix_1_1
            // 
            this.Matrix_1_1.Location = new System.Drawing.Point(157, 70);
            this.Matrix_1_1.Name = "Matrix_1_1";
            this.Matrix_1_1.Size = new System.Drawing.Size(100, 23);
            this.Matrix_1_1.TabIndex = 7;
            // 
            // Matrix_1_0
            // 
            this.Matrix_1_0.Location = new System.Drawing.Point(51, 70);
            this.Matrix_1_0.Name = "Matrix_1_0";
            this.Matrix_1_0.Size = new System.Drawing.Size(100, 23);
            this.Matrix_1_0.TabIndex = 6;
            // 
            // Matrix_2_4
            // 
            this.Matrix_2_4.Location = new System.Drawing.Point(475, 99);
            this.Matrix_2_4.Name = "Matrix_2_4";
            this.Matrix_2_4.Size = new System.Drawing.Size(100, 23);
            this.Matrix_2_4.TabIndex = 15;
            // 
            // Matrix_2_3
            // 
            this.Matrix_2_3.Location = new System.Drawing.Point(369, 99);
            this.Matrix_2_3.Name = "Matrix_2_3";
            this.Matrix_2_3.Size = new System.Drawing.Size(100, 23);
            this.Matrix_2_3.TabIndex = 14;
            // 
            // Matrix_2_2
            // 
            this.Matrix_2_2.Location = new System.Drawing.Point(263, 99);
            this.Matrix_2_2.Name = "Matrix_2_2";
            this.Matrix_2_2.Size = new System.Drawing.Size(100, 23);
            this.Matrix_2_2.TabIndex = 13;
            // 
            // Matrix_2_1
            // 
            this.Matrix_2_1.Location = new System.Drawing.Point(157, 99);
            this.Matrix_2_1.Name = "Matrix_2_1";
            this.Matrix_2_1.Size = new System.Drawing.Size(100, 23);
            this.Matrix_2_1.TabIndex = 12;
            // 
            // Matrix_2_0
            // 
            this.Matrix_2_0.Location = new System.Drawing.Point(51, 99);
            this.Matrix_2_0.Name = "Matrix_2_0";
            this.Matrix_2_0.Size = new System.Drawing.Size(100, 23);
            this.Matrix_2_0.TabIndex = 11;
            // 
            // Matrix_3_4
            // 
            this.Matrix_3_4.Location = new System.Drawing.Point(475, 128);
            this.Matrix_3_4.Name = "Matrix_3_4";
            this.Matrix_3_4.Size = new System.Drawing.Size(100, 23);
            this.Matrix_3_4.TabIndex = 20;
            // 
            // Matrix_3_3
            // 
            this.Matrix_3_3.Location = new System.Drawing.Point(369, 128);
            this.Matrix_3_3.Name = "Matrix_3_3";
            this.Matrix_3_3.Size = new System.Drawing.Size(100, 23);
            this.Matrix_3_3.TabIndex = 19;
            // 
            // Matrix_3_2
            // 
            this.Matrix_3_2.Location = new System.Drawing.Point(263, 128);
            this.Matrix_3_2.Name = "Matrix_3_2";
            this.Matrix_3_2.Size = new System.Drawing.Size(100, 23);
            this.Matrix_3_2.TabIndex = 18;
            // 
            // Matrix_3_1
            // 
            this.Matrix_3_1.Location = new System.Drawing.Point(157, 128);
            this.Matrix_3_1.Name = "Matrix_3_1";
            this.Matrix_3_1.Size = new System.Drawing.Size(100, 23);
            this.Matrix_3_1.TabIndex = 17;
            // 
            // Matrix_3_0
            // 
            this.Matrix_3_0.Location = new System.Drawing.Point(51, 128);
            this.Matrix_3_0.Name = "Matrix_3_0";
            this.Matrix_3_0.Size = new System.Drawing.Size(100, 23);
            this.Matrix_3_0.TabIndex = 16;
            // 
            // Matrix_4_4
            // 
            this.Matrix_4_4.Location = new System.Drawing.Point(475, 157);
            this.Matrix_4_4.Name = "Matrix_4_4";
            this.Matrix_4_4.Size = new System.Drawing.Size(100, 23);
            this.Matrix_4_4.TabIndex = 25;
            // 
            // Matrix_4_3
            // 
            this.Matrix_4_3.Location = new System.Drawing.Point(369, 157);
            this.Matrix_4_3.Name = "Matrix_4_3";
            this.Matrix_4_3.Size = new System.Drawing.Size(100, 23);
            this.Matrix_4_3.TabIndex = 24;
            // 
            // Matrix_4_2
            // 
            this.Matrix_4_2.Location = new System.Drawing.Point(263, 157);
            this.Matrix_4_2.Name = "Matrix_4_2";
            this.Matrix_4_2.Size = new System.Drawing.Size(100, 23);
            this.Matrix_4_2.TabIndex = 23;
            // 
            // Matrix_4_1
            // 
            this.Matrix_4_1.Location = new System.Drawing.Point(157, 157);
            this.Matrix_4_1.Name = "Matrix_4_1";
            this.Matrix_4_1.Size = new System.Drawing.Size(100, 23);
            this.Matrix_4_1.TabIndex = 22;
            // 
            // Matrix_4_0
            // 
            this.Matrix_4_0.Location = new System.Drawing.Point(51, 157);
            this.Matrix_4_0.Name = "Matrix_4_0";
            this.Matrix_4_0.Size = new System.Drawing.Size(100, 23);
            this.Matrix_4_0.TabIndex = 21;
            // 
            // DeterminantText
            // 
            this.DeterminantText.Location = new System.Drawing.Point(157, 212);
            this.DeterminantText.Name = "DeterminantText";
            this.DeterminantText.Size = new System.Drawing.Size(206, 23);
            this.DeterminantText.TabIndex = 26;
            // 
            // RandomMatrix
            // 
            this.RandomMatrix.Location = new System.Drawing.Point(678, 212);
            this.RandomMatrix.Name = "RandomMatrix";
            this.RandomMatrix.Size = new System.Drawing.Size(75, 23);
            this.RandomMatrix.TabIndex = 27;
            this.RandomMatrix.Text = "Random";
            this.RandomMatrix.UseVisualStyleBackColor = true;
            this.RandomMatrix.Click += new System.EventHandler(this.RandomMatrix_Click);
            // 
            // B_4
            // 
            this.B_4.Location = new System.Drawing.Point(597, 157);
            this.B_4.Name = "B_4";
            this.B_4.Size = new System.Drawing.Size(100, 23);
            this.B_4.TabIndex = 32;
            // 
            // B_3
            // 
            this.B_3.Location = new System.Drawing.Point(597, 128);
            this.B_3.Name = "B_3";
            this.B_3.Size = new System.Drawing.Size(100, 23);
            this.B_3.TabIndex = 31;
            // 
            // B_2
            // 
            this.B_2.Location = new System.Drawing.Point(597, 99);
            this.B_2.Name = "B_2";
            this.B_2.Size = new System.Drawing.Size(100, 23);
            this.B_2.TabIndex = 30;
            // 
            // B_1
            // 
            this.B_1.Location = new System.Drawing.Point(597, 70);
            this.B_1.Name = "B_1";
            this.B_1.Size = new System.Drawing.Size(100, 23);
            this.B_1.TabIndex = 29;
            // 
            // B_0
            // 
            this.B_0.Location = new System.Drawing.Point(597, 41);
            this.B_0.Name = "B_0";
            this.B_0.Size = new System.Drawing.Size(100, 23);
            this.B_0.TabIndex = 28;
            // 
            // X_4
            // 
            this.X_4.Location = new System.Drawing.Point(753, 157);
            this.X_4.Name = "X_4";
            this.X_4.Size = new System.Drawing.Size(100, 23);
            this.X_4.TabIndex = 37;
            // 
            // X_3
            // 
            this.X_3.Location = new System.Drawing.Point(753, 128);
            this.X_3.Name = "X_3";
            this.X_3.Size = new System.Drawing.Size(100, 23);
            this.X_3.TabIndex = 36;
            // 
            // X_2
            // 
            this.X_2.Location = new System.Drawing.Point(753, 99);
            this.X_2.Name = "X_2";
            this.X_2.Size = new System.Drawing.Size(100, 23);
            this.X_2.TabIndex = 35;
            // 
            // X_1
            // 
            this.X_1.Location = new System.Drawing.Point(753, 70);
            this.X_1.Name = "X_1";
            this.X_1.Size = new System.Drawing.Size(100, 23);
            this.X_1.TabIndex = 34;
            // 
            // X_0
            // 
            this.X_0.Location = new System.Drawing.Point(753, 41);
            this.X_0.Name = "X_0";
            this.X_0.Size = new System.Drawing.Size(100, 23);
            this.X_0.TabIndex = 33;
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(678, 241);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 38;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // FillSequence
            // 
            this.FillSequence.Location = new System.Drawing.Point(759, 212);
            this.FillSequence.Name = "FillSequence";
            this.FillSequence.Size = new System.Drawing.Size(75, 23);
            this.FillSequence.TabIndex = 39;
            this.FillSequence.Text = "Sequence";
            this.FillSequence.UseVisualStyleBackColor = true;
            this.FillSequence.Click += new System.EventHandler(this.FillSequence_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(289, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 15);
            this.label1.TabIndex = 40;
            this.label1.Text = "Matrix";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(639, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 15);
            this.label2.TabIndex = 41;
            this.label2.Text = "B";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(796, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 15);
            this.label3.TabIndex = 42;
            this.label3.Text = "X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(734, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 15);
            this.label4.TabIndex = 43;
            this.label4.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(734, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 15);
            this.label5.TabIndex = 44;
            this.label5.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(734, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 15);
            this.label6.TabIndex = 45;
            this.label6.Text = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(734, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 15);
            this.label7.TabIndex = 46;
            this.label7.Text = "3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(734, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 15);
            this.label8.TabIndex = 47;
            this.label8.Text = "4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(51, 216);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 15);
            this.label9.TabIndex = 48;
            this.label9.Text = "Determinant";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(579, 102);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 15);
            this.label10.TabIndex = 49;
            this.label10.Text = "=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 288);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FillSequence);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.X_4);
            this.Controls.Add(this.X_3);
            this.Controls.Add(this.X_2);
            this.Controls.Add(this.X_1);
            this.Controls.Add(this.X_0);
            this.Controls.Add(this.B_4);
            this.Controls.Add(this.B_3);
            this.Controls.Add(this.B_2);
            this.Controls.Add(this.B_1);
            this.Controls.Add(this.B_0);
            this.Controls.Add(this.RandomMatrix);
            this.Controls.Add(this.DeterminantText);
            this.Controls.Add(this.Matrix_4_4);
            this.Controls.Add(this.Matrix_4_3);
            this.Controls.Add(this.Matrix_4_2);
            this.Controls.Add(this.Matrix_4_1);
            this.Controls.Add(this.Matrix_4_0);
            this.Controls.Add(this.Matrix_3_4);
            this.Controls.Add(this.Matrix_3_3);
            this.Controls.Add(this.Matrix_3_2);
            this.Controls.Add(this.Matrix_3_1);
            this.Controls.Add(this.Matrix_3_0);
            this.Controls.Add(this.Matrix_2_4);
            this.Controls.Add(this.Matrix_2_3);
            this.Controls.Add(this.Matrix_2_2);
            this.Controls.Add(this.Matrix_2_1);
            this.Controls.Add(this.Matrix_2_0);
            this.Controls.Add(this.Matrix_1_4);
            this.Controls.Add(this.Matrix_1_3);
            this.Controls.Add(this.Matrix_1_2);
            this.Controls.Add(this.Matrix_1_1);
            this.Controls.Add(this.Matrix_1_0);
            this.Controls.Add(this.Matrix_0_4);
            this.Controls.Add(this.Matrix_0_3);
            this.Controls.Add(this.Matrix_0_2);
            this.Controls.Add(this.Matrix_0_1);
            this.Controls.Add(this.Matrix_0_0);
            this.Controls.Add(this.SolveButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Matrix Solver";
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private Button SolveButton;
    private TextBox Matrix_0_0;
    private TextBox Matrix_0_1;
    private TextBox Matrix_0_2;
    private TextBox Matrix_0_3;
    private TextBox Matrix_0_4;
    private TextBox Matrix_1_4;
    private TextBox Matrix_1_3;
    private TextBox Matrix_1_2;
    private TextBox Matrix_1_1;
    private TextBox Matrix_1_0;
    private TextBox Matrix_2_4;
    private TextBox Matrix_2_3;
    private TextBox Matrix_2_2;
    private TextBox Matrix_2_1;
    private TextBox Matrix_2_0;
    private TextBox Matrix_3_4;
    private TextBox Matrix_3_3;
    private TextBox Matrix_3_2;
    private TextBox Matrix_3_1;
    private TextBox Matrix_3_0;
    private TextBox Matrix_4_4;
    private TextBox Matrix_4_3;
    private TextBox Matrix_4_2;
    private TextBox Matrix_4_1;
    private TextBox Matrix_4_0;
    private TextBox DeterminantText;
    private Button RandomMatrix;
    private TextBox B_4;
    private TextBox B_3;
    private TextBox B_2;
    private TextBox B_1;
    private TextBox B_0;
    private TextBox X_4;
    private TextBox X_3;
    private TextBox X_2;
    private TextBox X_1;
    private TextBox X_0;
    private Button ClearButton;
    private Button FillSequence;
    private Label label1;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label label5;
    private Label label6;
    private Label label7;
    private Label label8;
    private Label label9;
    private Label label10;
}